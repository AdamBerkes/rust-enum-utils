#[macro_export]
macro_rules! create_enum {
    (
        @impl_enum $(#[$meta:meta])* $vis:vis enum $name:ident {
            $($(#[$vmeta:meta])* $vname:ident $(= $val:expr)?,)*
        }
    ) => {
        $(#[$meta])*
        $vis enum $name {
            $($(#[$vmeta])* $vname $(= $val)?,)*
        }

        impl std::convert::TryFrom<i32> for $name {
            type Error = ();

            fn try_from(v: i32) -> Result<Self, Self::Error> {
                match v {
                    $(x if x == $name::$vname as i32 => Ok(Self::$vname),)*
                    _ => Err(())
                }
            }
        }

        impl std::convert::TryFrom<&str> for $name {
            type Error = ();

            fn try_from(v: &str) -> Result<Self, Self::Error> {
                match v {
                    $(stringify!($vname) => Ok(Self::$vname),)*
                    _ => Err(())
                }
            }
        }
    };

    (
        $(#[$meta:meta])* $vis:vis enum $name:ident {
            $($(#[$vmeta:meta])* $vname:ident $(= $val:expr)?,)*
        }
    ) => {
        create_enum!(@impl_enum $(#[$meta:meta])* $vis enum $name {
            $($(#[$vmeta:meta])* $vname $(= $val)?,)*
        });
    };

    (
        $(#[$meta:meta])* $vis:vis enum $name:ident {
            $($(#[$vmeta:meta])* $vname:ident $(= $val:expr)?,)*
        }
        $(,AsStr)?
    ) => {
        create_enum!(@impl_enum $(#[$meta])* $vis enum $name {
            $($(#[$vmeta])* $vname $(= $val)?,)*
        });
        impl $name {
            pub fn as_str(&self) -> &str {
                match *self {
                    $(Self::$vname => stringify!($vname),)*
                }
            }
        }
    };
}
